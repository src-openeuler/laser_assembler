# laser_assembler

#### Description

Assorted filters designed to operate on 2D planar laser scanners, which use the sensor_msgs/LaserScan type.

#### Software Architecture
Software architecture description

The primary content of the laser_filters package is a number of general purpose filters for processing sensor_msgs/LaserScan messages. These filters are exported as plugins designed to work with with the filters package. At the moment all of these filters run directly on sensor_msgs/LaserScan, but filters may be added in the future which process sensor_msgs/PointCloud instead. Please review the filters documentation for an overview of how filters and filter chains are intended to work.

This package provides two nodes that can run multiple filters internally. Using these nodes to run your filters is considered best practice, since it allows multiple nodes to consume the output while only performing the filtering computation once. The nodes are minimal wrappers around filter chains of the given type. The scan_to_scan_filter_chain applies a series of filters to a sensor_msgs/LaserScan. The scan_to_cloud_filter_chain first applies a series of filters to a sensor_msgs/LaserScan, transforms it into a sensor_msgs/PointCloud, and then applies a series of filters to the sensor_msgs/PointCloud.


input:
```
laser_assembler/
├── CHANGELOG.rst
├── CMakeLists.txt
├── doc
│   └── data_flow.svg
├── examples
│   └── periodic_snapshotter.cpp
├── include
│   └── laser_assembler
├── package.xml
├── src
│   ├── laser_scan_assembler.cpp
│   ├── laser_scan_assembler_srv.cpp
│   ├── merge_clouds.cpp
│   ├── point_cloud2_assembler.cpp
│   ├── point_cloud_assembler
│   ├── point_cloud_assembler.cpp
│   └── point_cloud_assembler_srv.cpp
├── srv
│   ├── AssembleScans2.srv
│   └── AssembleScans.srv
└── test
    ├── dummy_scan_producer.cpp
    ├── test_assembler.cpp
    └── test_laser_assembler.launch

```

#### Installation

1.  Download RPM

aarch64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_aarch64/aarch64/ros-noetic-ros-laser_assembler/ros-noetic-ros-laser_assembler-1.7.8-1.oe2203.aarch64.rpm

x86_64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_x86_64/x86_64/ros-noetic-ros-laser_assembler/ros-noetic-ros-laser_assembler-1.7.8-1.oe2203.x86_64.rpm 

2.  Install RPM

aarch64:

sudo rpm -ivh ros-noetic-ros-laser_assembler-1.7.8-1.oe2203.aarch64.rpm --nodeps --force

x86_64:

sudo rpm -ivh ros-noetic-ros-laser_assembler-1.7.8-1.oe2203.x86_64.rpm --nodeps --force

#### Instructions

Dependence installation

sh /opt/ros/noetic/install_dependence.sh

Exit the following output file under the /opt/ros/noetic/ directory , Prove that the software installation is successful

```
laser_assembler/
├── cmake.lock
├── env.sh
├── include
│   └── laser_assembler
├── lib
│   ├── laser_assembler
│   ├── pkgconfig
│   └── python2.7
├── local_setup.bash
├── local_setup.sh
├── local_setup.zsh
├── setup.bash
├── setup.sh
├── _setup_util.py
├── setup.zsh
└── share
    ├── common-lisp
    ├── gennodejs
    ├── laser_assembler
    └── roseus

```

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
