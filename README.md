# laser_assembler

#### 介绍
laser_assembler设计用于在使用 sensor_msgs/LaserScan 类型的 2D 平面激光扫描仪上运行的各种过滤器

#### 软件架构
软件架构说明

laser_assembler包含以下方法：

laser_assembler包的主要内容是一些用于处理sensor_msgs/LaserScan消息的通用过滤器。这些过滤器作为插件导出，旨在与过滤器包一起使用。目前所有这些过滤器都直接在sensor_msgs/LaserScan上运行，但将来可能会添加过滤器来处理sensor_msgs/PointCloud。请查看过滤器文档以了解过滤器和过滤器链的工作原理。

这个包提供了两个可以在内部运行多个过滤器的节点。使用这些节点来运行过滤器被认为是最佳实践，因为它允许多个节点使用输出，同时只执行一次过滤计算。节点是给定类型的过滤器链周围的最小包装器。该scan_to_scan_filter_chain应用一系列的过滤器，以一个sensor_msgs /激光扫描。所述scan_to_cloud_filter_chain第一施加一系列的过滤器，以一个sensor_msgs /激光扫描，变换成一sensor_msgs /点云，然后应用一系列的过滤器到sensor_msgs /点云。

文件内容:
```
laser_assembler/
├── CHANGELOG.rst
├── CMakeLists.txt
├── doc
│   └── data_flow.svg
├── examples
│   └── periodic_snapshotter.cpp
├── include
│   └── laser_assembler
├── package.xml
├── src
│   ├── laser_scan_assembler.cpp
│   ├── laser_scan_assembler_srv.cpp
│   ├── merge_clouds.cpp
│   ├── point_cloud2_assembler.cpp
│   ├── point_cloud_assembler
│   ├── point_cloud_assembler.cpp
│   └── point_cloud_assembler_srv.cpp
├── srv
│   ├── AssembleScans2.srv
│   └── AssembleScans.srv
└── test
    ├── dummy_scan_producer.cpp
    ├── test_assembler.cpp
    └── test_laser_assembler.launch
```

#### 安装教程

1. 下载rpm包

aarch64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_aarch64/aarch64/ros-noetic-ros-laser_assembler/ros-noetic-ros-laser_assembler-1.7.8-1.oe2203.aarch64.rpm

x86_64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_x86_64/x86_64/ros-noetic-ros-laser_assembler/ros-noetic-ros-laser_assembler-1.7.8-1.oe2203.x86_64.rpm 
  
2. 安装rpm包

aarch64:

sudo rpm -ivh ros-noetic-ros-laser_assembler-1.7.8-1.oe2203.aarch64.rpm --nodeps --force

x86_64:

sudo rpm -ivh ros-noetic-ros-laser_assembler-1.7.8-1.oe2203.x86_64.rpm --nodeps --force

#### 使用说明

依赖环境安装:

sh /opt/ros/noetic/install_dependence.sh

安装完成以后，在/opt/ros/noetic/目录下有如下输出,则表示安装成功

输出:
```
laser_assembler/
├── cmake.lock
├── env.sh
├── include
│   └── laser_assembler
├── lib
│   ├── laser_assembler
│   ├── pkgconfig
│   └── python2.7
├── local_setup.bash
├── local_setup.sh
├── local_setup.zsh
├── setup.bash
├── setup.sh
├── _setup_util.py
├── setup.zsh
└── share
    ├── common-lisp
    ├── gennodejs
    ├── laser_assembler
    └── roseus

```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
